using NUnit.Framework;

namespace Dojo
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var first = 1;
            var second = 2;

            var result = DojoCode.AddTwoNums(first, second);

            Assert.AreEqual(result, 3);
        }
    }
}